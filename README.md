# fraction 

convert decimal into fraction

Haven't you ever wanted to print output is fraction rather than decimal?
It can be done with repeatedly take the reciprocal and remove the integer part, building what is called as `continued fraction`.

Here is the small example about how it works:

![Example][1]

#### HOW-TO USE

    $ make
    $ ./fract [decimal input]


[1]: http://bitbucket.org/rhoit/fraction/downloads/example.png
