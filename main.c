# include <math.h>
# include <stdio.h>
# include <stdlib.h> //for atof()

# define LIMIT 0.005

int temp=1;
int Dec2Fra(double mantissa){
  static int ret;
  
  printf("\t%9.7f", mantissa);
  
  int characteristic=(int)mantissa;
  mantissa-=characteristic;
  
  printf(" = %d + %9.7f\n", characteristic, mantissa);

  if(mantissa<LIMIT) return characteristic;
  
  ret=Dec2Fra(1/mantissa);
  printf("\t%d + %d/%d", characteristic, temp, ret);
  characteristic*=ret;
  characteristic+=temp;
  printf(" = %d/%d\n", characteristic, ret);
  temp=ret;
  return characteristic;
}

int main(int argc, char *argv[]) {
  double no;
  if(argc>1) no=atof(argv[1]); 
  else
  {
    printf("WARNING: missing argument, printing out the example\n");
    no=-66.0/47.0;
  }
   
  int n=(int)no;
   
  int numerator=Dec2Fra(fabs(no));
  if(no<0) printf("-");
  printf("%d", numerator);

  printf("/%d", temp);
  return 0;
}

